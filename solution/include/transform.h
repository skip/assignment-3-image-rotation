#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "image.h"
#include <stdint.h>
struct image rotate(struct image* const source, int64_t angle);
#endif
