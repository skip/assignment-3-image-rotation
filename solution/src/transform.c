#include "../include/image.h"
#include "../include/transform.h"
#include <stdio.h>
static struct image rotate_0(struct image* const source){
    uint64_t new_width = source->width;
    uint64_t new_height = source->height;
    struct image new_image = create_image(new_width, new_height);
    if(new_image.data == NULL) return zero_image;
    for (size_t h = 0; h < (source->height); ++h) {
        for (size_t w = 0; w < (source->width); ++w) {
            new_image.data[h * source->width + w] = source->data[h * source->width + w];
        }
    }
    return new_image;
}
static struct image rotate_90(struct image* const source){
    uint64_t new_width = source->height;
    uint64_t new_height = source->width;
    struct image new_image = create_image(new_width, new_height);
    if(new_image.data == NULL) return zero_image;
    for (size_t h = 0; h < (source->height); ++h) {
        for (size_t w = 0; w < (source->width); ++w) {
            new_image.data[((new_height - 1 - w) * new_width) + h] = source->data[(h * source->width) + w];
        }
    }
    return new_image;
}
static struct image rotate_180(struct image* const source){
    uint64_t new_width = source->width;
    uint64_t new_height = source->height;
    struct image new_image = create_image(new_width, new_height);
    if(new_image.data == NULL) return zero_image;
    for (size_t h = 0; h < (source->height); ++h) {
        for (size_t w = 0; w < (source->width); ++w) {
            new_image.data[((new_height - 1 - h) * new_width) + (new_width - 1 - w)] = source->data[(h * source->width) + w];
        }
    }
    return new_image;
}
static struct image rotate_270(struct image* const source){
    uint64_t new_width = source->height;
    uint64_t new_height = source->width;
    struct image new_image = create_image(new_width, new_height);
    if(new_image.data == NULL) return zero_image;
    for (size_t h = 0; h < (source->height); ++h) {
        for (size_t w = 0; w < (source->width); ++w) {
            new_image.data[(w * new_width) + (new_width - 1 - h)] = source->data[(h * source->width) + w];
        }
    }
    return new_image;
}
struct image rotate(struct image* const source, int64_t angle){
    switch (angle) {
        case 0:
            return rotate_0(source);
        case -270:
        case 90:
            return rotate_90(source);
        case -180:
        case 180:
            return rotate_180(source);
        case -90:
        case 270:
            return rotate_270(source);
        default:
            return *source;
    }
}
