#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(height * width * sizeof(struct pixel));
    if(img.data == NULL){
        printf("OUT OF MEMORY!");
        return zero_image;
    }
    return img;
}

struct image zero_image = {0};

void delete_image(struct image* img){
    img->width = 0;
    img->height = 0;
    if(img->data == NULL) return;
    free(img->data);
    img->data=NULL;
    
}

