#include "../include/bmp.h"
#include <stdio.h>
#define PAD 4
#define TYPE 0x4D42
#define BIT_COUNT 24
#define RESERVED 0
#define SIZE_H 40
#define PLANES 1
#define COMPRESSION 0
#define CORDS 0
#define COLOR_USED 0
#define COLOR_IMPORTANT 0

uint8_t count_padding(uint32_t width) {
    return (PAD - (width * sizeof(struct pixel)) % PAD);
}

enum read_status from_bmp(FILE* in, struct image* img){
    struct  bmp_header header;
    if(fread(&header, sizeof(struct bmp_header), 1, in ) != 1){
        fprintf(stderr, "Can't read file!\n");
        return READ_INVALID_SIGNATURE;
    }
    if(header.bfType != TYPE){
        fprintf(stderr, "This is not a code of BMP file!\n");
        return READ_TYPE_ERROR;
    }
    if(header.biBitCount != BIT_COUNT){
        fprintf(stderr, "Uncorrected bit count!\n");
        return READ_INVALID_BITS;
    }
    *img = create_image(header.biWidth, header.biHeight);
    for(uint32_t cnt = 0; cnt < (img -> height); ++cnt){
        if(fread(&img->data[cnt * header.biWidth], sizeof(struct pixel), header.biWidth, in) != header.biWidth){
            return READ_INVALID_SIGNATURE;
        }
        fseek(in, count_padding(img -> width), SEEK_CUR);
    }
    return READ_OK;

}


enum write_status to_bmp(FILE* out, const struct image* img){
    uint32_t padding = count_padding(img->width);
    uint32_t size_img= (sizeof(struct pixel) * img->width * img->height) + (padding * img->height);
    struct bmp_header header = {
            .biWidth = img->width,
            .biHeight = img->height,
            .bfType = TYPE,
            .biSizeImage = size_img,
            .bfileSize = (sizeof(struct bmp_header) + size_img),
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE_H,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biXPelsPerMeter = CORDS,
            .biYPelsPerMeter = CORDS,
            .biClrUsed = COLOR_USED,
            .biClrImportant = COLOR_IMPORTANT,
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    for (uint32_t cnt = 0; cnt < (img->height); ++cnt) {
        if (fwrite(&img->data[cnt * img->width], sizeof(struct pixel),img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}

