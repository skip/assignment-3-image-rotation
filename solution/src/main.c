#include "../include/bmp.h"
#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>

enum output_flags {
    SUCCESS = 0,
    ARG_ERR ,
    ANGLE_ERR,
    MEM_ERR,
    READ_ERR,
    WRITE_ERR,

};


int main(int argc, char* argv[]) {
    if(argc != 4){
        fprintf(stderr, "Incorrect input! Example for input: ./image-transformer <source-image> <transformed-image> <angle>");
        return ARG_ERR;
    }

    int64_t angle = atoi(argv[3]);
    if(angle < -270 || angle > 270 || angle % 90 != 0){
        fprintf(stderr, "Incorrect angle! Please input: 0, 90, -90, 180, -180, 270, -270!");
        return ANGLE_ERR;
    }


    FILE* image_src = fopen(argv[1], "rb");
    if(image_src == NULL){
        fprintf(stderr, "File not found!");
        return MEM_ERR;
    }

    struct image image;
    if(from_bmp(image_src, &image) != READ_OK){
        delete_image(&image);
        fclose(image_src);
        return READ_ERR;
    }

    struct image result = rotate(&image, angle);
    delete_image(&image);
    FILE* trans_img = fopen(argv[2], "wb");
    if(trans_img == NULL){
        fprintf(stderr, "Can't write to this file!");
        delete_image(&result);
        return MEM_ERR;
    }

    if(to_bmp(trans_img, &result) != WRITE_OK){
        fclose(trans_img);
        fclose(image_src);
        delete_image(&result);
        return WRITE_ERR;
    }


    fclose(image_src);
    fclose(trans_img);
    delete_image(&result);
    printf("Success!\n");
    return SUCCESS;
}
